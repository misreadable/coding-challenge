import './App.css';
import React from 'react';
import WindowSizeWatcher from "./components/WindowSizeWatcher";

class App extends React.Component {
  state = {
    value: localStorage.getItem("info") || "Default"
  };
  componentDidUpdate() {
    localStorage.setItem("info", this.state.value);
  }
  onChange = event => {
    this.setState({ value: event.target.value });
  };
  render() {
    const { value } = this.state;
    return (
        <div class="App" >
          <input value={value} type="text" onChange={this.onChange} />
          <p>Input value: {value}</p>
          <WindowSizeWatcher/>
        </div>
    );
  }
}

export default App;
