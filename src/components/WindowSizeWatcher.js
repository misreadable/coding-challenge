import useWindowSize from '../customHooks/useWindowSize';

const WindowSizeWatcher = () => {
    const size = useWindowSize();
    return (
        <div>
            {size.width}px / {size.height}px
        </div>
    );
}

export default WindowSizeWatcher;